#include <iostream>
#include <string>

using namespace std;
int main()
{
	// Important things
	string x = "X";
	string o = "O";
	int n = 6;
	int i;
	int y = 2;
	// Loop the loop
	while (n > 0)
	{
		// Here goes the magic ( 4 X's and O's)
		for (i = 8; i > 0; i--)
		{
			if (y % 2 == 0)
			{
				cout << x;
			}
			else if (y % 2 == 1)
			{
				cout << o;
			}
			y += 1;
		}
		cout << endl;
		y += 1;
		n -= 1;
	}

	system("pause");
	return 0;
}