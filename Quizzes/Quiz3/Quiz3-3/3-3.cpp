#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>
using namespace std;

int main()
{
	srand(time(0));
	// Important Things
	int x = 1;
	int y = 1;
	int m = 0;
	int r = 0;
	// Loop the Loop
	while (m != 4 && m != 8 && m != 12)
	{
		r = r + 1;
		cout << "Rolling ...." << endl;
		int xx = (rand() % 6) + 1;
		int yy = (rand() % 6) + 1;
		cout << xx << " , " << yy << endl;
		m = xx + yy;
		system("pause");
	}
	// Outcome
	cout << m << " is divisible by 4." << endl;
	cout << "You rolled " << r << " times." << endl;
	system("pause");
	return 0;
}