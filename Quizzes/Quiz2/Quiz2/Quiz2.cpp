#include <iostream>
#include <string>

using namespace std;

int main()
{
	// 
	
	float pi = 3.14f;
	float radius;

	//
	cout << "Area and circumference calculator" << endl;
	cout << "Enter the radius: ";
	cin >> radius;
	cout << "      " << endl;
	system("pause");

	//

	float circum = (2 * pi) * radius;
	float area = pi * (radius * radius);

	//
	cout << "    " << endl;
	cout << "The circumference is " << circum << endl;
	cout << "The area is " << area << endl;
	cout << "    " << endl;


	system("pause");
	return 0;
}