#include <iostream>
#include <string>

using namespace std;

int main()
{
	//
	string str = "Hello World!";
	cout << str << endl;
	
	//
	float x = 5.0f;
	int num = 65;

	//
	cout << x << endl;
	cout << "num = " << num << endl;

	system("pause");
	return 0;
}