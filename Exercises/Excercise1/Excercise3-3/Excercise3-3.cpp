#include <iostream>
#include <string>

using namespace std;

int main()
{

	//Var

	float money;
	const float php = 47.74f;

	//Input

	cout << "Enter how much USD you want to convert: ";
	cin >> money;
	system("pause");

	//Form

	float uphp = money * php;

	//Out

	cout << "  " << endl;
	cout << "Php is = " << php << endl;
	cout << money << " * " << php << " = " << uphp << endl;

	system("pause");
	return 0;
}