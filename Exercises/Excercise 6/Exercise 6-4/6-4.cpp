#include <iostream>
#include <string>

using namespace std;

int main()
{
	// Vari
	string pots[6] = { "Red Potion" , "Blue Potion" , "Phoenix Down" , "Panacea" , "Elixir" , "Red Potion" };
	string ufind;
	// Asking
	cout << "What are you looking for?: ";
	getline(cin, ufind);
	// Finding
	int index = -1;
	for (int i = 0; i < 6; i++)
	{
		if (ufind == pots[i])
		{
			index = i;
			break;

		}
	}
	// Checking
	if (index > -1)
	{
		cout << "Item '" << ufind << "' exists in the shop and is at index " << index << endl;
	}
	else
	{
		cout << "Item '" << ufind << "' does not exists in the shop." << endl;

	}
	system("pause");
	return 0;
}