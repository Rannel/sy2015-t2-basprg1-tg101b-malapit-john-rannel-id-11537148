#include <iostream>
#include <string>

using namespace std;

int main()
{
	// Vari
	string pots[6] = { "Red Potion" , "Blue Potion" , "Phoenix Down" , "Panacea" , "Elixir" , "Red Potion" };
	string ufind;
	int n = 0;
	// Asking
	cout << "What are you looking for?: ";
	getline(cin, ufind);
	// Finding
	int index = -1;
	for (int i = 0; i < 6; i++)
	{
		if (ufind == pots[i])
		{
			index = i;
			n = n + 1;
		}
	}
	// Checking
	if (index > -1)
	{
		cout << "Item '" << ufind << "' exists in the shop and has " << n << "x number of item" << endl;
	}
	else
	{
		cout << "Item '" << ufind << "' does not exists in the shop." << endl;

	}
	system("pause");
	return 0;
}