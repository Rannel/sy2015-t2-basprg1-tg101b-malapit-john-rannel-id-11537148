#include <iostream>
#include <string>


using namespace std;

int main()
{

	//
	float maxhp;
	float curhp;

	//
	cout << "Enter your max hp: ";
	cin >> maxhp;
	cout << "Enter your current hp: ";
	cin >> curhp;

	//
	float prcnt = curhp / maxhp;
	float one = 1.0f;
	float fifty = .50f;
	float twofive = .25f;
	float zero = .00f;

	//
	if (prcnt == one)
	{
		cout << "Your hp is full." << endl;
	}

	else if (prcnt > fifty)
	{
		cout << "Your hp is on green" << endl;
	}

	else if (prcnt > twofive)
	{
		cout << "Your hp is yellow" << endl;
	}

	else if (prcnt > zero)
	{
		cout << "Your hp is red." << endl;
	}

	else
	{
		cout << "You are dead" << endl;
	
	}

	system("pause");
	return 0;

}


// cout << "Your input is invalid." >> endl;
// system("pause");
// return 0;
// string status;