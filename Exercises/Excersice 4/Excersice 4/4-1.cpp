#include <iostream>
#include <string>

using namespace std;

int main()
{

	//
	int x;
	int y;

	//
	cout << "Enter your first number: ";
	cin >> x;
	cout << "Enter your 2nd number: ";
	cin >> y;
	system("pause");

	//

	bool a = x == y;
	bool b = x != y;
	bool c = x < y;
	bool d = x > y;
	bool e = x <= y;
	bool f = x >= y;

	//
	cout << "   " << endl;
	cout << "0 means false/no and 1 means true/yes." << endl;
	cout << "    " << endl;
	cout << "Your 1st number is " << a << " equal to your 2nd number " << endl;
	cout << "Your 1st number is " << b << " not equal to your 2nd number " << endl;
	cout << "Your 1st number is " << c <<	 " less than your 2nd number " << endl;
	cout << "Your 1st number is " << e << " less than or equal to your 2nd number " << endl;
	cout << "Your 1st number is " << d << " greater than to your 2nd number " << endl;
	cout << "Your 1st number is " << f << " greater than or equal to your 2nd number " << endl;
	
	system("pause");
	return 0;
}