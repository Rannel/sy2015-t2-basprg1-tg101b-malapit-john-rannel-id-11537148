#include <iostream>
#include <string>

using namespace std;

void main()
{
	float celsius = 0;

	cout << "Current Celsius is " << celsius << "." << endl;
	cout << "Enter temperature you want to convert into Fahrenheit: ";
	cin >> celsius;

	float fahrenheit = (9.0 / 5.0) * celsius + 32;

	cout << "Converted temperature is " << fahrenheit << "F." << endl;

	system("pause");
}