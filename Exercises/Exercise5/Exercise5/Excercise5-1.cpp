#include <iostream>
#include <string>

using namespace std;
int main()
{
	int n;
	int x;
	int sm = 0;
	float av;

	cout << "The total numbers that you want to put: ";
	cin >> n;

	for (int y = 0; y < n; y++)
	{
		cout << "Input the number/s: ";
		cin >> x;
		sm += x;
	}
	av = (float)sm / n;

	cout << "The sum is " << sm << " and the average is " << av << " ." << endl;

	system("pause");
	return 0;
}