#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>
using namespace std;

int main()
{
	srand(time(0));
	std::string uchoice;
	// List of Inputs
	int uhp;
	int ehp;
	int maxdu;
	int mindu;
	int maxde;
	int minde;
	// Player inputting the data needed
	cout << "Enter your total HP: ";
	cin >> uhp;
	cout << "Enter your maximum damage: ";
	cin >> maxdu;
	cout << "Enter your minimum damage: ";
	cin >> mindu;
	system("cls");
	cout << "Enter enemy's total HP: ";
	cin >> ehp;
	cout << "Enter the enemy's maximum damage: ";
	cin >> maxde;
	cout << "Enter enemy's minimum damage: ";
	cin >> minde;
	system("cls");
	// This the one who checks if the datas are correct
	while (uhp <= -0 || ehp <= 0 || maxdu < mindu || maxde < minde)
	{
		cout << "Enter the wrong data and this will continue forever." << endl;
		cout << "The 'correct' datas would be: HP is more than 0 and maximum damage is more than minimum damage." << endl;
		cout << "Enter your total HP: ";
		cin >> uhp;
		cout << "Enter your maximum damage: ";
		cin >> maxdu;
		cout << "Enter your minimum damage: ";
		cin >> mindu;
		cout << "Enter enemy's total HP: ";
		cin >> ehp;
		cout << "Enter the enemy's maximum damage: ";
		cin >> maxde;
		cout << "Enter enemy's minimum damage: ";
		cin >> minde;
		system("cls");
	}
	// System, saying the it all
	cout << "-------" << endl;
	cout << "Your total HP is " << uhp << ", your maximum damage is " << maxdu << ", and your minimum damage is " << mindu << "." << endl;
	cout << "Enemy total HP is " << ehp << ", the maximum damage is " << maxde << ", and the minimum damage is " << minde << "." << endl;
	system("pause");
	system("cls");
	// Gameplay
	while (uhp > 0 && ehp > 0)
	{
		// Attack, wild attack, and defend damage computation for both player and enemy + AI
		int au = (rand() % (maxdu - mindu + 1)) + mindu;
		int ae = (rand() % (maxde - minde + 1)) + minde;
		int awu = au * 2;
		int awe = ae * 2;
		int adu = au / 2;
		int ade = ae / 2;
		int ai = (rand() % 3) + 1;
		//Battle system
		cout << "Note* You can only input 1 action at a time, and it needs to be in small letters." << endl;
		cout << "Your HP is: " << uhp << " || " << "Enemy HP is: " << ehp << endl;
		cout << "[a] Attack, [w] Wild attack, and [d] Defend." << endl;
		cout << "What would be your action: " << endl;
		cin >> uchoice;
		//When the AI used attack and you chose a/w/d effects
		if (ai == 1 && uchoice == "a")
		{
			system("cls");
			uhp -= ae;
			ehp -= au;
			cout << "You used attack." << endl;
			cout << "Enemy have received " << au << " damage." << endl;
			cout << "Enemy has " << ehp << " HP left." << endl;
			cout << endl;
			cout << "Enemy used attack." << endl;
			cout << "You have received " << ae << " damage." << endl;
			cout << "You have " << uhp << " HP left." << endl;
			cout << "   " << endl;
		}
		else if (ai == 1 && uchoice == "w")
		{
			system("cls");
			uhp -= ae;
			ehp -= awu;
			cout << "You used wild attack." << endl;
			cout << "Enemy have received " << awu << " damage." << endl;
			cout << "Enemy has " << ehp << " HP left." << endl;
			cout << endl;
			cout << "Enemy used attack." << endl;
			cout << "You have received " << ae << " damage." << endl;
			cout << "You have " << uhp << " HP left." << endl;
			cout << "   " << endl;
		}
		else if (ai == 1 && uchoice == "d")
		{
			system("cls");
			uhp -= ade;
			cout << "You used defend." << endl;
			cout << "Enemy have received 0 damage." << endl;
			cout << "Enemy has " << ehp << " HP left." << endl;
			cout << endl;
			cout << "Enemy used attack." << endl;
			cout << "You have received " << ade << " damage." << endl;
			cout << "You have " << uhp << " HP left." << endl;
			cout << endl;
		}
		//When the AI uses wild attack and you chose a/w/d effects
		else if (ai == 2 && uchoice == "a")
		{
			system("cls");
			uhp -= awe;
			ehp -= au;
			cout << "You used attack." << endl;
			cout << "Enemy have received " << au << " damage." << endl;
			cout << "Enemy has " << ehp << " HP left." << endl;
			cout << endl;
			cout << "Enemy used wild attack." << endl;
			cout << "You have received " << awe << " damage." << endl;
			cout << "You have " << uhp << " HP left." << endl;
			cout << "   " << endl;
		}
		else if (ai == 2 && uchoice == "w")
		{
			system("cls");
			uhp -= awe;
			ehp -= awu;
			cout << "You used wild attack." << endl;
			cout << "Enemy have received " << awu << " damage." << endl;
			cout << "Enemy has " << ehp << " HP left." << endl;
			cout << endl;
			cout << "Enemy used wild attack." << endl;
			cout << "You have received " << awe << " damage." << endl;
			cout << "You have " << uhp << " HP left." << endl;
			cout << "   " << endl;

		}
		else if (ai == 2 && uchoice == "d")
		{
			system("cls");
			ehp -= awu;
			cout << "You used defend." << endl;
			cout << "Enemy have received " << awu << " damage." << endl;
			cout << "Enemy has " << ehp << " HP left." << endl;
			cout << endl;
			cout << "Enemy used wild attack." << endl;
			cout << "You have received 0 damage." << endl;
			cout << "You have " << uhp << " HP left." << endl;
			cout << endl;
		}
		//When the AI used defend and you chose a/w/d effects
		else if (ai == 3 && uchoice == "a")
		{
			system("cls");
			ehp -= adu;
			cout << "You used attack" << endl;
			cout << "Enemy has received " << adu << " damage." << endl;
			cout << "Enemy has " << ehp << " HP left." << endl;
			cout << endl;
			cout << "Enemy uses defend." << endl;
			cout << "You have received 0 damage." << endl;
			cout << "You have " << uhp << " HP left." << endl;
			cout << endl;
		}
		else if (ai == 3 && uchoice == "w")
		{
			system("cls");
			uhp -= awe;
			cout << "You used wild attack." << endl;
			cout << "Enemy recieved 0 damage." << endl;
			cout << "Enemy has " << ehp << " HP left." << endl;
			cout << endl;
			cout << "Enemy uses defend." << endl;
			cout << "You have received " << awe << " damage." << endl;
			cout << "You have " << uhp << " HP left." << endl;
			cout << endl;
		}
		else if (ai == 3 && uchoice == "d")
		{
			system("cls");
			cout << "You used defend." << endl;
			cout << "Enemy received 0 damage." << endl;
			cout << " Enemy has " << ehp << " HP left." << endl;
			cout << endl;
			cout << "Enemy uses defend." << endl;
			cout << "You have received 0 damage." << endl;
			cout << "You have " << uhp << " HP left." << endl;
			cout << "   " << endl;
		}
		system("pause");
		system("cls");
	}
	//Results of the battle.
	if (uhp <= 0 && ehp >= 0)
	{
		cout << "You died." << endl;
		cout << "The enemy won." << endl;
	}
	else if (uhp >= 0 && ehp <= 0)
	{
		cout << "The enemy died." << endl;
		cout << "You won." << endl;
	}
	else if (uhp <= 0 && ehp <= 0)
	{
		cout << "You both died." << endl;
		cout << "Shinigami: You guys are an idiot huh?" << endl;
	}
	system("pause");
	return 0;
}